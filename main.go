package main

import (
	"fmt"
	"log"
	"net/http"

	"github.com/gorilla/websocket"
)

var upgrader = websocket.Upgrader{
	ReadBufferSize:  1024,
	WriteBufferSize: 1024,
}

func WsReader(conn *websocket.Conn) {
	for {
		messageType, message, err := conn.ReadMessage()
		if err != nil {
			log.Println(err)
			return
		}
		log.Println(string(message))

		if err := conn.WriteMessage(messageType, message); err != nil {
			log.Println(err)
			return
		}
	}
}

func HomePage(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "Hello from Golang developer Azizbek, This is homepage")
}

func WebsocketEndPoint(w http.ResponseWriter, r *http.Request) {
	upgrader.CheckOrigin = func(r *http.Request) bool { return true }

	ws, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		log.Println(err)
		return
	}

	log.Println("Successfully connected to web socket")

	WsReader(ws)
}

func SetUpRoute() {
	http.HandleFunc("/", HomePage)
	http.HandleFunc("/ws", WebsocketEndPoint)
}

func main() {
	fmt.Println("Quick starting with websockets")
	SetUpRoute()
	log.Fatal(http.ListenAndServe(":8080", nil))
}
